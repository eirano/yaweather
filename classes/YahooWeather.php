<?php

class YahooWeather
{

    public $locale = "ru-RU";

    private $yql_url = "http://query.yahooapis.com/v1/public/yql";
    private $cities_url = "https://www.yahoo.com/news/_td/api/resource/WeatherSearch;text=";

    /**
     * @var string degree param for yql
     */
    public $degree = 'c';

    /**
     * @var string city name called
     */

    public $cityName;
    /**
     * @var string woeid called
     */
    public $woeid;

    /**
     * @var string forecast for city
     */

    public $forecast;

    public function __construct ( $woeid, $cityName ) {
        $this->woeid = $woeid;
        $this->cityName = $cityName;
    }

    /**
     * @param $string
     * @return mixed|null
     */
    public function searchCities($string)
    {
        // Make call with cURL
        $session = curl_init($this->cities_url . urlencode($string) . "?lang=" . $this->locale);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($session);

        return $result;
    }


    /**
     * @return mixed
     */
    public function getCityWeather()
    {

        $yql_query = "select * from weather.forecast where woeid = " . $this->woeid . " and u='" . $this->degree . "'";
        $yql_query_url = $this->yql_url . "?q=" . urlencode($yql_query) . "&format=json";


        // Make call with cURL
        $session = curl_init($yql_query_url);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($session);

        // Convert JSON to PHP object
        $phpObj = json_decode($result);

        $this->forecast = $phpObj->query->results->channel->item->forecast;

        return $phpObj;
    }


    /**
     * @param $array
     * @return mixed
     */
    public function getWeather($array)
    {
        $comma_separated = implode(",", $array);

        $yql_query = "select * from weather.forecast where woeid in (" . $comma_separated . ") and u='" . $this->degree . "'";
        $yql_query_url = $this->yql_url . "?q=" . urlencode($yql_query) . "&format=json&lang=ru-RU";

        // Make call with cURL
        $session = curl_init($yql_query_url);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($session);

        // Convert JSON to PHP object
        $phpObj = json_decode($result);
        return $phpObj;
    }


    /**
     *
     */
    public function renderCityForecastHTML()
    {

        $degree = $this->degree == 'c' ? "&#8451;" : "&#8457;";
        $forecast = $this->forecast;

        $html = '<table class="table"><tbody>';
        foreach ($forecast as $item) {
            $fdate = DateTime::createFromFormat('j M Y', $item->date);

            $html .= '<tr>';
            $html .= '<td>' . strftime("%A", $fdate->getTimestamp()) . '</td>';
            $html .= '<td><img src="http://us.i1.yimg.com/us.yimg.com/i/us/nws/weather/gr/' . $item->code . 'd.png" width="32"></td>';
            $html .= '<td>' . $item->high . $degree . '</td>';
            $html .= '<td class="text-muted">' . $item->low . $degree . '</td></tr>';
        };
        $html .= '</tbody></table>';
        $html .= '<button type="button" class="btn btn-primary" id="addCityButton" data-attr-id="' . $this->woeid . '" data-attr-name="' . $this->cityName . '">Добавить</button>';

        echo $html;
    }


}