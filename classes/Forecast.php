<?php

/**
 * Created by PhpStorm.
 * User: eirano
 * Date: 16.09.16
 * Time: 13:48
 */
class Forecast
{
    private $db;

    public function __construct()
    {
        $this->db = Db::getInstance();
    }

    /**
     * @param null $avg
     * @return mixed
     */
    function getStoredCitiesForecast($avg = null)
    {

        if ($avg != null && is_int($avg)) {
            $this->db->query("select * from forecast where woeid in (
                            SELECT woeid
                            FROM  (
                                SELECT distinct(woeid), avg(high) as avg_h from forecast group by woeid
                            ) sub where avg_h > " . $avg . "
                    )");
        } else {
            $this->db->query("SELECT * FROM forecast order by city, date");
        }

        return $this->db->resultset();
    }


    /**
     * @param $service
     * @param $woeid
     * @param $name
     */
    function storeForecast($service, $woeid, $name)
    {

        $forecast = $service->forecast;
        $this->db->beginTransaction();

        foreach ($forecast as $item) {
            $fdate = DateTime::createFromFormat('j M Y', $item->date)->getTimestamp();

            $this->db->query('INSERT INTO forecast (woeid, city, code, date, day, high, low) VALUES (:woeid, :city, :code, :date, :day, :high, :low)');

            $this->db->bind(':woeid', $woeid);
            $this->db->bind(':city', $name);
            $this->db->bind(':code', $item->code);
            $this->db->bind(':date', $item->date);
            $this->db->bind(':day', strftime("%A", $fdate));
            $this->db->bind(':high', $item->high);
            $this->db->bind(':low', $item->low);

            $this->db->execute();

        }

        $this->db->endTransaction();

        $errors = $this->db->getErrors();
        if (count($errors) > 0) {
            print_r($errors);
        }
    }

    /**
     * @param $woeid
     */
    function removeForecast($woeid)
    {
        $this->db->query('DELETE FROM forecast where woeid = :woeid');
        $this->db->bind(':woeid', $woeid);
        $this->db->execute();

        $errors = $this->db->getErrors();
        if (count($errors) > 0) {
            print_r($errors);
        }
    }


    /**
     * @param $rows
     * @return string
     */
    function renderStoredCitiesHTML($rows)
    {

        $html = "";
        foreach ($rows as $key => $val) {
            $html .= '<div class="city"><h2>' . $key . '</h2>';
            $html .= '<table class="table"><tbody>';

            foreach ($val as $item => $v) {

                $html .= '<tr>';
                $html .= '<td>' . $v['day'] . '</td>';
                $html .= '<td><img src="http://us.i1.yimg.com/us.yimg.com/i/us/nws/weather/gr/' . $v['code'] . 'd.png" width="32"></td>';
                $html .= '<td>' . $v['high'] . '</td>';
                $html .= '<td class="text-muted">' . $v['low'] . '</td></tr>';
            };

            $html .= '</tbody></table>';
            $html .= '<button type="button" class="btn btn-danger" id="removeCityButton" data-attr-id="' . $v['woeid'] . '">Удалить</button></div>';
        }
        return $html;
    }


    /**
     * @param $result
     * @return string
     */
    function renderStoredCitiesHTMLCards($result)
    {

        foreach ($result as $row => $v) {
            $rows[$v['city']][] = $v;
        }

        $html = "";
        foreach ($rows as $key => $val) {
            $html .= '<div class="city"><h2>' . $key . '</h2>';
            $html .= '<div class="row">';

            foreach ($val as $item => $v) {
                $html .= '<div class="col-md-2 col-xs-12">';
                $html .= '<div class="card card-block">';
                $html .= '<img class="card-img-top" src="http://us.i1.yimg.com/us.yimg.com/i/us/nws/weather/gr/' . $v['code'] . 'd.png" width="64" alt="">';
                $html .= '<div class="card-block">';
                $html .= '<h5 class="card-title"><b>' . $v['day'] . '</b></h5>';
                $html .= '<p class="card-text">день: ' . $v['high'] . '<br />ночь: ' . $v['low'] . '</p>';
                $html .= '</div>';
                $html .= '</div>';
                $html .= '</div>';
            };

            $html .= '</div>';
            $html .= '<button type="button" class="btn btn-danger" id="removeCityButton" data-attr-id="' . $v['woeid'] . '">Удалить город</button></div>';
        }
        return $html;
    }

}