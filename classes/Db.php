<?php

class Db
{

    private $dsn = "pgsql:host=localhost;dbname=weather;";
    private $user = "me";
    private $pass = "me";

    private $_db;
    static $_instance;

    private $errors = array();

    private $stmt;

    public function __construct()
    {

        // Set options
        $options = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        );

        // Create a new PDO instance
        try {
            $this->_db = new PDO($this->dsn, $this->user, $this->pass, $options);
        } // Catch any errors
        catch (PDOException $e) {
            $this->errors[] = $e->getMessage();
        }

    }

    public static function getInstance()
    {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }


    /**
     * @param $query
     */
    public function query($query)
    {
        $this->stmt = $this->_db->prepare($query);
    }

    /**
     * @param $param
     * @param $value
     * @param null $type
     */
    public function bind($param, $value, $type = null)
    {
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }
        $this->stmt->bindValue($param, $value, $type);
    }


    /**
     * @return mixed
     */
    public function execute()
    {
        try {
            return $this->stmt->execute();
        } catch (PDOException $e) {
            $this->errors[] = $e->getMessage();
        }
    }

    /**
     * @return mixed
     */
    public function resultset()
    {
        $this->execute();
        return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
    }


    /**
     * @return bool
     */
    public function beginTransaction()
    {
        return $this->_db->beginTransaction();
    }

    /**
     * @return bool
     */
    public function endTransaction()
    {
        return $this->_db->commit();
    }

    /**
     * @return bool
     */
    public function cancelTransaction()
    {
        return $this->_db->rollBack();
    }

    /**
     * @return string
     */
    public function getErrors()
    {
        return $this->errors;
    }
}