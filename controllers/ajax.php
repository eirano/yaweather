<?php

setlocale(LC_ALL, 'ru_RU.UTF-8');
require_once("../classes/Db.php");
require_once("../classes/YahooWeather.php");
require_once("../classes/Forecast.php");


$function = filter_input(INPUT_POST, 'function');


switch ($function) {

    case "searchCity":

        $search = filter_input(INPUT_POST, 'search');
        header('Content-Type: application/json');

        $service = new YahooWeather();
        echo $service->searchCities($search);
        break;


    case "showCityWeather":

        $woeid = filter_input(INPUT_POST, 'woeid', FILTER_VALIDATE_INT);
        $cityName = filter_input(INPUT_POST, 'city_name');

        $service = new YahooWeather($woeid, $cityName);
        $service->getCityWeather();
        $service->renderCityForecastHTML();
        break;


    case "removeCity":

        $woeid = filter_input(INPUT_POST, 'woeid', FILTER_VALIDATE_INT);

        $mForecast = new Forecast();
        $mForecast->removeForecast($woeid);
        break;


    case "storeCity":

        $woeid = filter_input(INPUT_POST, 'woeid', FILTER_VALIDATE_INT);
        $cityName = filter_input(INPUT_POST, 'city_name');

        $service = new YahooWeather($woeid, $cityName);
        $service->getCityWeather();

        $mForecast = new Forecast();
        $mForecast->storeForecast($service, $service->woeid, $service->cityName);
        break;
}