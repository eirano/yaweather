<?php

require_once("classes/Db.php");
require_once("classes/Forecast.php");

$filter = false;
$mForecast = new Forecast();

$avg = filter_input(INPUT_GET, 'avg', FILTER_VALIDATE_INT);
$result = $mForecast->getStoredCitiesForecast($avg);

$storedCitiesHTML = 'Городов не найдено';


if (count($result) > 0) {
    $storedCitiesHTML = $mForecast->renderStoredCitiesHTMLCards($result);
    $filter = true;
}

?>

<!DOCTYPE html>
<html lang="en-US">
<head>
    <script src="vendor/components/jquery/jquery.min.js"></script>
    <script src="vendor/twbs/bootstrap/dist/js/bootstrap.js"></script>
    <script src="assets/js/bootstrap-typeahead.js"></script>
    <link href="vendor/twbs/bootstrap/dist/css/bootstrap.css" rel="stylesheet">

    <script src="assets/js/main.js"></script>
    <link href="assets/css/style.css" rel="stylesheet">
</head>


<body>
<div class="wrap">

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <label>Введите название населенного пункта</label>
                <input type="text" name="city" class="form-control">
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h2 id="title"></h2>
                <div id="details"></div>
            </div>
        </div>
    </div>

    <div class="container">
        <?php if ($filter) : ?>
            <div class="row">
                <div class="col-md-12">
                    <form class="form-inline">
                        <div class="form-group">
                            <label for="exampleInputName2">Вы можете отобрать города, где дневная температура больше среднего указанного значения: </label>
                            <input type="text" class="form-control" name="avg" value="<?=$avg;?>">
                        </div>
                        <button type="submit" class="btn btn-primary">Отобрать</button>
                    </form>
                </div>
            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-md-12">
                <?= $avg ? '<div class="alert alert-info" role="alert">Показаны города со средней температурой больше ' . $avg . ' градусов <a class="btn btn-info" role="button" href="index.php">сбросить</a></div>' : '' ?>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12 ">
                <h2 id="title"></h2>
                <div><?= $storedCitiesHTML; ?></div>
            </div>
        </div>
    </div>
</div>

</body>
</html>

