### How do I get set up? ###

In your web-folder folder run this command:

```
#!bash

git clone https://eirano@bitbucket.org/eirano/yaweather.git
```


Then type in command line:

```
#!bash

composer install
```



After that run this queries in your postgresql:

```
#!sql

CREATE DATABASE weather
  WITH OWNER = me
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'en_US.UTF-8'
       LC_CTYPE = 'en_US.UTF-8'
       CONNECTION LIMIT = -1;

create table forecast (
  woeid numeric,
  city varchar(255) not null,
  code numeric,
  date varchar(64) not null,
  day varchar(16) not null,
  high numeric,
  low numeric,

  primary key (woeid, city, date)
);
```

Change the database connection settings in file 

```
#!bash

/{path_to_project}/classes/Db.php
```



```
#!php

private $dsn = "pgsql:host={localhost};dbname={database_name};";
private $user = "{database_user}";
private $pass = "{database_user_password}";
```

