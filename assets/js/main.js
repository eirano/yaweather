$(document).ready(function () {

    $('input[name="city"]').typeahead({
        onSelect: function (item) {
            var name = (item.text).split(',').slice(-3, 2).join();

            $("#title").text(name);
            getCityWeather(item.value, name);

        },
        ajax: {
            url: 'controllers/ajax.php',
            timeout: 100,
            displayField: "qualifiedName",
            valueField: "woeid",
            triggerLength: 1,
            method: "post",
            dataType: "JSON",
            preDispatch: function (query) {
                return {
                    search: query,
                    function: 'searchCity'
                }
            },
            preProcess: function (data) {

                if (data.success === false) {
                    return false;
                } else {
                    return data;
                }
            }
        }
    });

    function getCityWeather(woeid, name) {
        $.ajax({
            method: "POST",
            url: "controllers/ajax.php",
            data: {function: "showCityWeather", woeid: woeid, city_name: name}
        }).done(function (html) {
            $("#details").html(html);
            //$("#details").append('<button type="button" class="btn btn-primary" id="addCityButton" data-attr-id="' + woeid + '" data-attr-name="' + name + '">Добавить</button>');
        });
    }

    $(document).on("click", "#addCityButton", function () {

        var city_id = $(this).attr("data-attr-id");
        var city_name = $(this).attr("data-attr-name");

        $.ajax({
            method: "POST",
            url: "controllers/ajax.php",
            data: {function: "storeCity", woeid: city_id, city_name: city_name}
        }).done(function (data) {

            location.reload();
        });
    });


    $(document).on("click", "#removeCityButton", function () {

        var city_id = $(this).attr("data-attr-id");
        var citydata = $(this).parent();

        $.ajax({
            method: "POST",
            url: "controllers/ajax.php",
            data: {function: "removeCity", woeid: city_id}
        }).done(function () {
            citydata.slideUp();
        });

    });


});